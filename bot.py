import sys
import time
import concurrent.futures
import threading

from ftx import FtxClient


class TicketProvider(object):
    def __init__(self):
        self.value = 0
        self._lock = threading.Lock()

    def increment(self):
        with self._lock:
            self.value += 1


class Settings:
    buy_pair: str
    token_name: str
    loop_time_in_ms: float
    api_key: str
    secret: str

    def __init__(self, parameters) -> None:
        self.initialize_properties(parameters)
        self.print_properties()

    def initialize_properties(self, parameters) -> None:
        print('Initializing parameters...')
        self.buy_pair = parameters[1]
        self.token_name = parameters[2]
        self.api_key = parameters[3]
        self.secret = parameters[4]

    def print_properties(self):
        print("=================================================================================================")
        print("Parameters initialized:")
        print(f"API KEY: {self.api_key}")
        print(
            f"SECRET LENGTH: {len(self.secret)} Secret: {self.secret[0:3]}***{self.secret[-3:]}")
        print(f"Buy Pair: {self.buy_pair}")
        print(f"Token: {self.token_name}")
        print("=================================================================================================")


class Bot:

    exchange: FtxClient
    settings: Settings
    market: str
    free_buy_pair = 0.0
    locked_buy_pair = 0.0
    total_buy_pair = 0.0
    total_token_amount = 0.0
    amount_to_use_for_buy = 0.0

    #futures = []

    def __init__(self, parameters):
        self.settings = parameters
        print('Connection is in progres..')
        self.exchange = FtxClient(self.settings.api_key, self.settings.secret)
        self.market = self.settings.token_name + '/' + self.settings.buy_pair
        print(self.market)
        self.refresh_wallet_balance()
        time.sleep(5)

    def update_balance_async(self, executor):
        #executor.submit(self.refresh_balance_async, i * 0.05)
        return

    def execute_threads_for_orders(self, executor):
        for i in range(10):
            executor.submit(self.place_buy_order, i * 0.1)
        return

    def refresh_balance_async(self, delay):
        time.sleep(delay)
        start = time.time()
        self.refresh_wallet_balance()
        end = time.time()
        print(f"Took {end-start} seconds to make single request.")
        return True

    def refresh_wallet_balance(self):
        # get all current balance from the wallet
        all_balances = self.exchange.get_balances()
        print(f"Updated balance with {len(all_balances)} tokens")

        for asset in all_balances:
            if asset["coin"] == self.settings.buy_pair:
                self.free_buy_pair = asset['free']
                self.free_buy_pair = float(int(self.free_buy_pair))
                self.total_buy_pair = asset['total']
                print(
                    "============================IMPORTANT==============================")
                self.amount_to_use_for_buy = self.free_buy_pair

                print(
                    f"Current buy amount is {self.amount_to_use_for_buy} {self.settings.buy_pair}.")
                print(
                    "============================IMPORTANT==============================")
                self.check_current_free_buy_pair_ratio()

    def check_current_free_buy_pair_ratio(self):
        if (self.free_buy_pair == 0):
            print("You have 0 amount of buy pair.")
            return
        ratio = self.free_buy_pair / self.total_buy_pair
        self.ratio_percentage = ratio * 100
        print(
            f"BuyAmountFree: {self.free_buy_pair} TOKEN: {self.settings.token_name}: {self.total_token_amount}")

    def place_buy_order(self, delay):
        time.sleep(delay)
        print(
            f"Placing a market buy order on {self.market} with {self.free_buy_pair}")
        self.exchange.place_market_buy_order(
            self.market, self.amount_to_use_for_buy)
        return


if __name__ == '__main__':
    print('Bot starting...')
    print('--------------')
    print(sys.argv)
    settings = Settings(sys.argv)
    bot = Bot(settings)

    while(True):
        try:
            executor = concurrent.futures.ThreadPoolExecutor(max_workers=10)
            bot.execute_threads_for_orders(executor)
            time.sleep(0.1)
        except KeyboardInterrupt:
            print("KeyboardInterrupt has caught")
            executor.shutdown(wait=False, cancel_futures=True)
            break
