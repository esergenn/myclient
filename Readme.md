# Çok Önemli

1. Uygulamanın aldığı ilk parametre buy-pair (USD USDT) olarak kullanılır. Cüzdanda bulunan tüm parayı kullanır. Herhangi bir şekilde limitleyemezsiniz. Alımı test etmek isterseniz önceliklde buy-pair olarak kullanacağınız miktarı borsada azaltabilirsiniz.

* 10 dolar gibi küçük miktarlara USDT alarak buy-pair olarak bunu kullanabilirsiniz. (Tavsiye edilen yöntem)
* USD ile denemek isterseniz, teste sokmak istemediğiniz miktarı geçici olarak USDT ye taşıyabilirsiniz. (Al-Sat yaparken ufak miktarlarda kaybınız olabilir)

2. Uygulama çalıştırıldığında, cüzdanı `SADECE 1 KERE` günceller ve alım miktarını belirler. Uygulama açıldıktan sonra cüzdanınızda bir değişiklik olursa, yeniden başlatmanız gerekir. Yoksa emirler yeterli bakiye yok şeklinde hata döndürür ve alım yapamazsınız. Uygulamayı açıldıktan sonra buy-pair (USD veya USDT) miktarınızı değiştirmeyin

3. Uygulamaya parametre geçerken tek tırnak(') yerine, çift tırnak(") kullanın. Serverdaki ubuntu üzerinde nasıl davranacağını bilmiyorum ancak, windowsda tek tırnak, parametreleri wrap etmiş gibi davranmadığı için string e dahil oluyor.

# Nasıl Kullanılır?

FTX websitesinde, ayarlardaki API bölümünden API-KEY key generate edilir. API-KEY read only olursa, al-sat yapamadığınız için uygulama çalışmaz.

Serverda, uygulamanın bulunduğu klasör repo ismiyle aynı: `myclient`

`cd myclient` kullanarak bot.py dosyasının olduğu klasöre girilir.

Şu parametrelerle uygulama başlatılır:

`python3 bot.py "buy-pair" "token-name" "api-key" "secret"`

USD ile OXY almak istediğinizde kullanacağınız örnek:

`python3 bot.py "USD" "OXY" "T66GzNULaHHmQyW12312dsafsaMty5QzDtyU46jb" "RxtkXU1PYshVzE2536184fasdaf0DmEW0L"`

## API-KEY veya Secret yanlış olduğunda alacağınız hata:

`Not-logged-in`

## Read Only olan API-KEY ile çalıştırırsanız alacağınız response:

![ReadOnly](doc-resources/NotAllowedReadOnly.png)

## Uygulama düzgün şekilde çalıştığında ve market hala açık değilken alacağınız response:

![NoSuchMarket](doc-resources/NoSuchMarket.png)

## Uygulama alım yaptığında alacağınız response:

![Success](doc-resources/Success.png)